(function ($) {

  $(document).ready(function () {

    $(".select-user").click(function() {

      $("#edit-username").val($(this).text()).effect( 'highlight', { }, 1500 );
      $('#edit-submit').focus();

    });

  });

})(jQuery);
